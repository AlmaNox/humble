﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ApiConnector;
using ApiConnector.Models;
using BitfinexClient.Models;

namespace BterClient
{
    public class InformationBterClient : InformationClient
    {
        private static string apiUrl = @"http://data.bter.com/api/1/";

        public InformationBterClient()
        {
            Culture = new CultureInfo(1);
        }

        private CultureInfo Culture { get; set; }

        public ICollection<Offert> Bids(Currency Source, Currency Destination)
        {
            string url = apiUrl + @"depth/";
            var client = new RestClient(url + ConvertCurrency(Source, Destination));
            string s = client.MakeRequest();
            var response = (Bids) MySerializer.DeserializeObjectFromJson(typeof (Bids), s);
            return MapBidsToOfferts(response.List);
        }

        public ICollection<Offert> Asks(Currency Source, Currency Destination)
        {
            string url = apiUrl + @"depth/";
            var client = new RestClient(url + ConvertCurrency(Source, Destination));
            string s = client.MakeRequest();
            var response = (Asks) MySerializer.DeserializeObjectFromJson(typeof (Asks), s);
            return MapAsksToOfferts(response.List);
        }

        private ICollection<Offert> MapAsksToOfferts(ICollection<string[]> collection)
        {
            ICollection<Offert> offerts = new List<Offert>(collection.Count);
            foreach (var ask in collection)
            {
                offerts.Add(new Offert
                {
                    Amount = Decimal.Parse(ask[1], Culture.NumberFormat),
                    Price = Decimal.Parse(ask[0], Culture.NumberFormat)
                });
            }
            return offerts;
        }

        private ICollection<Offert> MapBidsToOfferts(ICollection<string[]> collection)
        {
            ICollection<Offert> offerts = new List<Offert>(collection.Count);
            foreach (var bid in collection)
            {
                offerts.Add(new Offert
                {
                    Amount = Decimal.Parse(bid[1], Culture.NumberFormat),
                    Price = Decimal.Parse(bid[0], Culture.NumberFormat)
                });
            }
            return offerts;
        }

        private string ConvertCurrency(Currency firstCurrency, Currency secondCurrency)
        {
            return ConvertCurrencyToString(firstCurrency) + "_" + ConvertCurrencyToString(secondCurrency);
        }

        private string ConvertCurrencyToString(Currency currency)
        {
            switch (currency)
            {
                case Currency.USD:
                    return "usd";
                case Currency.BTC:
                    return "btc";
                case Currency.LTC:
                    return "ltc";
            }
            throw new Exception();
        }
    }
}