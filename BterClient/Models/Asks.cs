﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BitfinexClient.Models
{
    [DataContract]
    public class Asks
    {
        [DataMember(Name = "asks")]
        public ICollection<string[]> List { get; set; }
    }
}