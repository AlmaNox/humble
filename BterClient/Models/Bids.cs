﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BitfinexClient.Models
{
    [DataContract]
    public class Bids
    {
        [DataMember(Name = "bids")]
        public ICollection<string[]> List { get; set; }
    }
}