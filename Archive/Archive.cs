﻿using ApiConnector.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Historia
{
    /*
     *  Klasa ma za zadanie zbierac informacje z konkretnych zapytan i jej metoda aktualizuj ma byc wywolywana 
     *  po pobraniu kazdego zapytania
     *  Tak jakby dla kazdego zapytania asks/bids z kazdej gieldy
     *  ma byc utworzony jeden obiekt tej klasy tego rodzaju zapytania.
     *  Obiekt zostanie zainicjalizowany wraz z pierwszym zapytaniem danego rodzaju
     *  jesli pole tej klasy bedzie puste zostanie utworzonewraz z pierwsza aktualizacja ma zostac utworzony
     *  
     * Ponadto obiekty tej klasy beda wywalaly dane, ktore BitUpgrader bedzie przetwarzal w ciezszych
     * obliczniowo statystykach
     * i wywalal komunitkaty
     * 
     */
    public class Archive
    {
        private int przeszlosc = 10;
        private ICollection<ICollection<Offert>> listazapytan;
        private List<DateTime> czas;
   
        private String path;
        private FileStream fs;
        public String Typee { get; set; }

        public void Update(ICollection<Offert> offerts, DateTime value)
        {
           if (czas==null)
           {
               czas = new List<DateTime>();
           }
            czas.Add(value);
            fs = File.Open(path, FileMode.Append);
            AddTime(fs, value);

            if (listazapytan == null)
            {
                listazapytan = new List<ICollection<Offert>>();
            }
            listazapytan.Add(offerts);
           // System.IO.File.AppendAllLines(path, offerts.GetEnumerator(), Encoding.UTF8);
            AddText(fs, offerts);
            if (listazapytan.Count > przeszlosc)
            {
                listazapytan.ElementAt(0).Clear(); 
            }
            fs.Close();

               
        }


        public ICollection<ICollection<Offert>> getarchiwum()
        {
            return listazapytan;
        }
        public void showall( int pozycje, int zapytania)
        {
           int zap = czas.Count < zapytania ? czas.Count : zapytania; 
           for (int i=0; i<zap; i++)
           {
               Console.WriteLine(czas.ElementAt(i));
            //   foreach (Offert off in listazapytan.ElementAt(i))
               var oferty = listazapytan.ElementAt(i);
               for (int y=0; y<pozycje; y++)
               {
                   var off = oferty.ElementAt(y);
                   Console.WriteLine("Ilość: "+off.Amount+ " Cena: " +off.Price/* + " Rezultat: " + off.Result + " Msg: "+ off.Msg */);
                   
               }
           }
        }
        public void Setpast(int a)
        {
            przeszlosc = a;
        }
        public int Getpast(int a)
        {
            return przeszlosc;
        }

        public Archive(String t)
        {
            Typee = t;
            path = Environment.CurrentDirectory;
               path += "\\archiwa\\" + t + ".txt";

              
            
                 
           

       
            
        }


        public Decimal CostByAmount(Decimal aa)
        {
            Decimal a = 0;
            Decimal p = 0;
            foreach (Offert off in listazapytan.Last())
            {
                a += off.Amount;
                if (a > aa)
                {
                    p += off.Price * (off.Amount- (a - aa));
                    break;
                }
                else p += off.Price*off.Amount;
               
               

            }
            return p;
        }


        private void AddText(FileStream fs, ICollection<Offert> offerts)
        {
            String tekst;
            byte[] info;
            foreach (Offert offert in offerts)
            {
                info = new UTF8Encoding(true).GetBytes(offert.Amount + " " + offert.Price + "\r\n");
                fs.Write(info, 0, info.Length);
            }
            info = new UTF8Encoding(true).GetBytes("\r\n");
            fs.Write(info, 0, info.Length);
            
           
        }
        private void AddTime(FileStream fs, DateTime value)
        {
            byte[] info = new UTF8Encoding(true).GetBytes( value.ToString() + "\r\n");
            fs.Write(info, 0, info.Length);
        }
    }
}
