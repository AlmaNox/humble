﻿using ApiConnector;
using ApiConnector.Models;
using BitfinexClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class BitfinexClientTest
    {
        private static TradeClient Tc { get; set; }


        [ClassInitialize]
        public static void MyClassInitialize(TestContext testContext)
        {
            if (Tc != null) return;
            Tc = new TradeBitfinexClient("bGKb7aA42DRGV3CvSDGtwLvpL3Mi7gJKl08Gww0a9Ea",
                "HJBk6V9ewfr8si16SqfVE5zFJbV7xzORHuVlJpq0uap");
            AutomapperConfiguration.Configure();
        }

        [TestMethod]
        public void MakeNewOrderTest()
        {
            NewOrder test = Tc.MakeNewOrder(Currency.LTC, Currency.BTC,
                new decimal(1000), new decimal(1000), true);
            Assert.AreEqual(Result.Ok, test.Result);
        }

        [TestMethod]
        public void CancelOrderTest()
        {
            ActionResult test = Tc.CancelOrder(-1);
            Assert.AreEqual(Result.Wrong, test.Result);
        }

        [TestMethod]
        public void CancelAllOrdersTest()
        {
            ActionResult test = Tc.CancelAllOrders();
            Assert.AreEqual(Result.Ok, test.Result);
        }

        [TestMethod]
        public void GetOrderStatusTest()
        {
            OrderInfo test = Tc.GetOrderStatus(0);
            Assert.AreEqual(Result.Ok, test.Result);
        }

        [TestMethod]
        public void GetAllOrdersStatusTest()
        {
            OrdersStatus test = Tc.GetAllOrdersStatus();
            Assert.AreEqual(Result.Ok, test.Result);
        }

        [TestMethod]
        public void GetNewDepositAddressTest()
        {
            DepositInfo test = Tc.GetNewDepositAddress(Currency.BTC);
            Assert.AreEqual(Result.Ok, test.Result);
        }

        [TestMethod]
        public void GetBalancesTest()
        {
            WalletsStatus test = Tc.GetBalances();
            Assert.IsTrue(test.Wallets.Keys.Contains(new WalletMetadata
            {
                Currency = Currency.BTC,
                Type = WalletType.Deposit
            }));

            Assert.AreEqual(Result.Ok, test.Result);
        }
    }
}