﻿using System;
using System.Runtime.Serialization;
using ApiConnector;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [DataContract(Name = "People")]
    internal class People
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "yaer")]
        public int Year { get; set; }
    }

    [TestClass]
    public class MySerializerTest
    {
        [TestMethod]
        public void SerializationTest()
        {
            var p = new People {Name = "Test", Year = 1};
            String excepted = "{\"name\":\"Test\",\"yaer\":1}";
            String test = MySerializer.SerializeObjectToJson(p);
            Assert.AreEqual(excepted, test);
        }

        [TestMethod]
        public void DeserializationTest()
        {
            String p = "{\"name\":\"Test\",\"yaer\":1}";
            var excepted = new People {Name = "Test", Year = 1};
            var test = (People) MySerializer.DeserializeObjectFromJson(typeof (People), p);
            Assert.AreEqual(excepted.Name, test.Name);
            Assert.AreEqual(excepted.Year, test.Year);
        }
    }
}