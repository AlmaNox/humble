﻿using System;

namespace ApiConnector.Models
{
    public enum Result
    {
        Ok,
        Wrong,
        Unknown
    }

    public class ActionResult
    {
        public ActionResult(Result result = Result.Ok)
        {
            Result = result;
        }

        public Result Result { get; set; }
        public String Msg { get; set; }
    }
}