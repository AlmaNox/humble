﻿using System;

namespace ApiConnector.Models
{
    /// <summary>
    /// </summary>
    public class Offert : ActionResult
    {
        public Decimal Price { get; set; }
        public Decimal Amount { get; set; }
    }
}