﻿using System.Collections.Generic;

namespace ApiConnector.Models
{
    public class OrdersStatus : ActionResult
    {
        public IDictionary<int, OrderInfo> Orders { get; set; }
    }
}