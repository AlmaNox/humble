﻿using System;

namespace ApiConnector.Models
{
    public class WalletInfo
    {
        public Decimal Amount { get; set; }
        public Decimal AvaibleAmount { get; set; }
    }
}