﻿using System;
using System.Collections.Generic;

namespace ApiConnector.Models
{
    public enum WalletType
    {
        Exchange,
        Deposit,
        Unknown
    }

    public class WalletMetadata : IEquatable<WalletMetadata>
    {
        public WalletType Type { get; set; }
        public Currency Currency { get; set; }

        public bool Equals(WalletMetadata other)
        {
            return other.Currency == Currency && other.Type == Type;
        }


        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var objasMetadata = obj as WalletMetadata;
            return objasMetadata != null && Equals(objasMetadata);
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode() + (Currency.GetHashCode()*10);
        }
    }

    public class WalletsStatus : ActionResult
    {
        public IDictionary<WalletMetadata, WalletInfo> Wallets { get; set; }
    }
}