﻿using System;

namespace ApiConnector.Models
{
    public class OrderInfo : ActionResult
    {
        public Currency CurrencyFirst { get; set; }
        public Currency CurrencySecond { get; set; }
        public Decimal Price { get; set; }
        public Decimal OrginalAmount { get; set; }
        public Decimal ExecutedAmount { get; set; }
        public Decimal RemainingAmount { get; set; }
        public Boolean Buy { get; set; }
        public Boolean Alive { get; set; }
    }
}