﻿using System;

namespace ApiConnector.Models
{
    public class DepositInfo : ActionResult
    {
        public Currency Currency { get; set; }
        public String Address { get; set; }
    }
}