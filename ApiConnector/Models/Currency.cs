﻿namespace ApiConnector.Models
{
    /// <summary>
    /// </summary>
    public enum Currency
    {
        USD,
        BTC,
        LTC,
        DRK,
        Unknown
    }
}