﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

public enum HttpVerb
{
    GET,
    POST,
    PUT,
    DELETE
}

namespace ApiConnector
{
    public class RestClient
    {
        public RestClient()
        {
            Constructor("", HttpVerb.GET, "");
        }

        public RestClient(string endpoint)
        {
            Constructor(endpoint, HttpVerb.GET, "");
        }

        public RestClient(string endpoint, HttpVerb method)
        {
            Constructor(endpoint, method, "");
        }

        public RestClient(string endpoint, HttpVerb method, string postData)
        {
            Constructor(endpoint, method, postData);
        }

        public string EndPoint { get; set; }
        public HttpVerb Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }
        public IDictionary<string, string> Headers { get; set; }

        private void Constructor(string endpoint, HttpVerb method, string postData)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = "text/json";
            PostData = postData;
            Headers = new Dictionary<string, string>();
        }


        public string MakeRequest()
        {
            return MakeRequest("");
        }

        public void AddHeader(string header, string contents)
        {
            Headers.Add(header, contents);
        }

        public string MakeRequest(string parameters)
        {
            var request = (HttpWebRequest) WebRequest.Create(EndPoint + parameters);

            request.KeepAlive = true;
            request.Method = Method.ToString();

            request.ContentType = ContentType;


            //request.PreAuthenticate();

            var headers = new NameValueCollection();
            if (Headers.Count != 0)
            {
                foreach (var header in Headers)
                {
                    headers.Add(header.Key, header.Value);
                    //request.Headers.Add(header.Key, header.Value);
                }
            }

            request.Headers.Add(headers);

            if (!string.IsNullOrEmpty(PostData) && Method == HttpVerb.POST)
            {
                var encoding = new UTF8Encoding();
                byte[] bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);
                request.ContentLength = bytes.Length;

                using (Stream writeStream = request.GetRequestStream())
                {
                    writeStream.Write(bytes, 0, bytes.Length);
                }
            }
            try
            {
                using (var response = (HttpWebResponse) request.GetResponse())
                {
                    string responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }

                    // grab the response
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }

                    return responseValue;
                }
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    using (Stream responseStream = ex.Response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            string responseValue = reader.ReadToEnd();
                            return responseValue;
                        }
                    }
                }
                throw ex;
            }
        }
    }
}