﻿using System.Collections.Generic;
using ApiConnector.Models;

namespace ApiConnector
{
    /// <summary>
    /// </summary>
    public interface InformationClient
    {
        /// <summary>
        /// </summary>
        /// <param name="Source"></param>
        /// <param name="Destination"></param>
        /// <returns></returns>
        ICollection<Offert> Bids(Currency Source, Currency Destination);

        ICollection<Offert> Asks(Currency Source, Currency Destination);
    }
}