﻿using ApiConnector.Models;

namespace ApiConnector
{
    public interface TradeClient
    {
        /// <summary>
        ///     Zaklada nowe zlecenie.
        /// </summary>
        /// <param name="firstCurrency">Waluta, którą wymieniamy. Dalej zwana FC.</param>
        /// <param name="secondCurrency">Waluta będąca środkiem płatniczym w tej wymianie. Dalej zwana SC.</param>
        /// <param name="amount">Ilość waluty pierwszej.</param>
        /// <param name="price">
        ///     Po jakiej cenie chcemy wymienić pierwszą walutę liczoną w cenie drugiej waluty. Tj. ile kosztuje
        ///     jedna jednostka FC w SC.
        /// </param>
        /// <param name="buy">Prawda oznacza, że chcemy kupić FC, a fałsz sprzedaż.</param>
        /// <returns></returns>
        NewOrder MakeNewOrder(Currency firstCurrency, Currency secondCurrency, decimal amount, decimal price, bool buy);

        /// <summary>
        ///     Anulowanie zlecenia.
        /// </summary>
        /// <param name="orderId">Pobierany przy zakładaniu zlecenia albo przez metode GetAllOrdersStatus.</param>
        /// <returns></returns>
        ActionResult CancelOrder(int orderId);

        /// <summary>
        ///     Anuluje wszytkie zlecenia.
        /// </summary>
        /// <returns></returns>
        ActionResult CancelAllOrders();

        /// <summary>
        ///     Zwraca informacje na temat zlecenia.
        /// </summary>
        /// <param name="orderId">Pobierany przy zakładaniu zlecenia albo przez metode GetAllOrdersStatus.</param>
        /// <returns></returns>
        OrderInfo GetOrderStatus(int orderId);

        /// <summary>
        ///     Zwraca listę par: id jako klucz i informacje o zleceniu, którego id jest w kluczu.
        /// </summary>
        /// <returns></returns>
        OrdersStatus GetAllOrdersStatus();

        /// <summary>
        /// </summary>
        /// <param name="currency"></param>
        /// <returns></returns>
        DepositInfo GetNewDepositAddress(Currency currency);

        /// <summary>
        /// </summary>
        /// <returns></returns>
        WalletsStatus GetBalances();
    }
}