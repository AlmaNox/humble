﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using AutoMapper.Internal;

namespace ApiConnector
{
    public class MySerializer
    {
        public static Object DeserializeObjectFromJson(Type responseType, string request)
        {
            object response = Activator.CreateInstance(responseType);
            if (!responseType.IsListOrDictionaryType() && request.StartsWith("["))
            {
                request = "{\"collection\" : " + request + "} ";
            }
            try
            {
                using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(request)))
                {
                    var serializer = new DataContractJsonSerializer(responseType);
                    response = serializer.ReadObject(ms);
                }
            }
            catch (Exception ex)
            {
                request = "{\"message\" : \"" + request + "\"} ";
                using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(request)))
                {
                    var serializer = new DataContractJsonSerializer(responseType);
                    response = serializer.ReadObject(ms);
                }
            }
            return response;
        }

        public static string SerializeObjectToJson<Type>(Type request)
        {
            using (var ms = new MemoryStream())
            {
                var deserializer = new DataContractJsonSerializer(typeof (Type));
                deserializer.WriteObject(ms, request);
                ms.Position = 0;
                var sr = new StreamReader(ms);
                return sr.ReadToEnd();
            }
        }
    }
}