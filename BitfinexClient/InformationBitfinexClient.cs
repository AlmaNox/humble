﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ApiConnector;
using ApiConnector.Models;
using BitfinexClient.Models;

namespace BitfinexClient
{
    /// <summary>
    /// </summary>
    public class InformationBitfinexClient : InformationClient
    {
        public InformationBitfinexClient()
        {
            Culture = new CultureInfo(1);
        }

        private CultureInfo Culture { get; set; }

        ICollection<Offert> InformationClient.Bids(Currency source, Currency destination)
        {
            string url = @"https://api.bitfinex.com/v1/book/";
            var client = new RestClient(url + ConvertCurrency(source) + ConvertCurrency(destination));
            string s = client.MakeRequest();
            var response = (Bids) MySerializer.DeserializeObjectFromJson(typeof (Bids), s);
            return MapBidsToOfferts(response.List);
        }

        public ICollection<Offert> Asks(Currency Source, Currency Destination)
        {
            string url = @"https://api.bitfinex.com/v1/book/";
            var client = new RestClient(url + ConvertCurrency(Source) + ConvertCurrency(Destination));
            string s = client.MakeRequest();
            var response = (Asks) MySerializer.DeserializeObjectFromJson(typeof (Asks), s);
            return MapAsksToOfferts(response.List);
        }

        public string ConvertCurrency(Currency currency)
        {
            switch (currency)
            {
                case Currency.USD:
                    return "usd";
                case Currency.BTC:
                    return "btc";
                case Currency.LTC:
                    return "ltc";
            }
            throw new Exception();
        }

        private ICollection<Offert> MapAsksToOfferts(ICollection<Ask> asks)
        {
            ICollection<Offert> offerts = new List<Offert>(asks.Count);
            foreach (Ask ask in asks)
            {
                offerts.Add(new Offert
                {
                    Amount = Decimal.Parse(ask.Amount, Culture.NumberFormat),
                    Price = Decimal.Parse(ask.Price, Culture.NumberFormat)
                });
            }
            return offerts;
        }

        private ICollection<Offert> MapBidsToOfferts(ICollection<Bid> bids)
        {
            ICollection<Offert> offerts = new List<Offert>(bids.Count);
            foreach (Bid bid in bids)
            {
                offerts.Add(new Offert
                {
                    Amount = Decimal.Parse(bid.Amount, Culture.NumberFormat),
                    Price = Decimal.Parse(bid.Price, Culture.NumberFormat)
                });
            }
            return offerts;
        }
    }
}