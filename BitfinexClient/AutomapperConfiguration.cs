﻿using System;
using System.Collections.Generic;
using ApiConnector.Models;
using AutoMapper;
using BitfinexClient.Models.Response;

namespace BitfinexClient
{
    public class AutomapperConfiguration
    {
        /// <summary>
        ///     Mapping
        ///     NewDepositResopnse to DepositInfo
        ///     ResponseInfo to ActionResult
        ///     GetBalancesResponse to WalletsStatus
        ///     OrderResponse to NewOrder
        ///     CancelAllOrdersResponse to ActionResult
        ///     OrderResponse to OrderInfo
        ///     AllOrderResponse to OrdersStatus
        ///     CancelOrderResponse to ActionResult
        /// </summary>
        public static void Configure()
        {
            // NewDepositResopnse to DepositInfo
            AddCommonMapping(
                Mapper.CreateMap<NewDepositResponse, DepositInfo>()).
                ForMember(d => d.Address, r => r.MapFrom(src => src.Address)).
                ForMember(d => d.Currency, r => r.ResolveUsing(src => CurrencyConvertFromString(src.Currency)
                    ));

            // ResponseInfo to ActionResult
            Mapper.CreateMap<ResponseInfo, ActionResult>().
                ForMember(d => d.Result,
                    r =>
                        r.ResolveUsing(
                            src => String.IsNullOrEmpty(src.Message) ? Result.Ok : Result.Wrong)).
                ForMember(d => d.Msg, r => r.MapFrom(src => src.Message));

            // GetBalancesResponse to WalletsStatus
            AddCommonMapping(
                Mapper.CreateMap<GetBalancesResponse, WalletsStatus>()).
                ForMember(d => d.Wallets, r => r.ResolveUsing(src =>
                {
                    IDictionary<WalletMetadata, WalletInfo> wallets = new Dictionary<WalletMetadata, WalletInfo>();
                    foreach (WalletInformation wallet in src.Wallets)
                    {
                        wallets.Add(new KeyValuePair<WalletMetadata, WalletInfo>(
                            new WalletMetadata
                            {
                                Currency = CurrencyConvertFromString(wallet.Currency),
                                Type = wallet.Type.CompareTo("deposit") == 0
                                    ? WalletType.Deposit
                                    : wallet.Type.CompareTo("exchange") == 0 ? WalletType.Exchange : WalletType.Unknown
                            },
                            new WalletInfo
                            {
                                Amount = Convert.ToDecimal(wallet.Amount),
                                AvaibleAmount = Convert.ToDecimal(wallet.Available)
                            }
                            ));
                    }
                    return wallets;
                }));

            // OrderResponse to NewOrder
            AddCommonMapping(
                Mapper.CreateMap<OrderResponse, NewOrder>()).ForMember(d => d.OrderId, r => r.MapFrom(src => src.Order));

            // CancelAllOrdersResponse to ActionResult
            Mapper.CreateMap<CancelAllOrdersResponse, ActionResult>().
                ForMember(d => d.Result,
                    r =>
                        r.ResolveUsing(
                            src => src.Message.CompareTo("All orders cancelled") == 0 ? Result.Ok : Result.Wrong)).
                ForMember(d => d.Msg, r => r.MapFrom(src => src.Message));

            // OrderResponse to OrderInfo
            AddCommonMapping(
                Mapper.CreateMap<OrderResponse, OrderInfo>()).
                ForMember(d => d.Alive, r => r.MapFrom(src => src.IsLive)).
                ForMember(d => d.Buy, r => r.ResolveUsing(src => src.Side == "buy" ? true : false)).
                ForMember(d => d.CurrencyFirst, r => r.ResolveUsing(src => GetFirstCurrency(src.Symbol))).
                ForMember(d => d.CurrencySecond, r => r.ResolveUsing(src => GetSecondCurrency(src.Symbol))).
                ForMember(d => d.ExecutedAmount, r => r.ResolveUsing(src => Convert.ToDecimal(src.ExecutedAmount))).
                ForMember(d => d.OrginalAmount, r => r.ResolveUsing(src => Convert.ToDecimal(src.OrginialAmount))).
                ForMember(d => d.RemainingAmount, r => r.ResolveUsing(src => Convert.ToDecimal(src.RemainingAmount))).
                ForMember(d => d.Price, r => r.ResolveUsing(src => Convert.ToDecimal(src.Price)));

            // AllOrderResponse to OrdersStatus
            AddCommonMapping(
                Mapper.CreateMap<AllOrdersResponse, OrdersStatus>()).
                ForMember(d => d.Orders, r => r.MapFrom(src => src.Orders));
            // CancelOrderResponse to ActionResult
            AddCommonMapping(
                Mapper.CreateMap<CancelOrderResponse, ActionResult>());
        }

        private static IMappingExpression<TSource, TDestination> AddCommonMapping<TSource, TDestination>(
            IMappingExpression<TSource, TDestination> m)
            where TSource : ResponseInfo
            where TDestination : ActionResult
        {
            return m.
                ForMember(d => d.Result,
                    r =>
                        r.ResolveUsing(
                            src => String.IsNullOrEmpty(src.Message) ? Result.Ok : Result.Wrong)).
                ForMember(d => d.Msg, r => r.MapFrom(src => src.Message));
        }

        private static Currency CurrencyConvertFromString(string currency)
        {
            return currency == "btc"
                ? Currency.BTC
                : currency == "ltc"
                    ? Currency.LTC
                    : currency == "usd"
                        ? Currency.USD
                        : currency == "drk"
                            ? Currency.DRK
                            : Currency.Unknown;
        }

        private static Currency GetFirstCurrency(string symbol)
        {
            return CurrencyConvertFromString(symbol.Substring(0, 3));
        }

        private static Currency GetSecondCurrency(string symbol)
        {
            return CurrencyConvertFromString(symbol.Substring(4, 3));
        }
    }
}