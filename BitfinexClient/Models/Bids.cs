﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BitfinexClient.Models
{
    [DataContract]
    public class Bids
    {
        [DataMember(Name = "bids")]
        public ICollection<Bid> List { get; set; }
    }
}