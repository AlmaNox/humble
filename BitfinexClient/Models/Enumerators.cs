﻿namespace BitfinexClient.Models
{
    public enum Wallet
    {
        trading,
        exchange,
        deposit
    }

    public enum Side
    {
        buy,
        sell,
        unknown
    }

    public enum OrderType
    {
        market,
        limit,
        stop,
        trailing_stop,
        fill_or_kill,
        echange_market,
        echange_limit,
        echange_stop,
        echange_trailing_stop,
        echange_fill_or_kill
    }
}