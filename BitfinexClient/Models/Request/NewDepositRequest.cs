﻿using System.Runtime.Serialization;

namespace BitfinexClient.Models.Request
{
    [DataContract]
    internal class NewDepositRequest : Request
    {
        [DataMember(Name = "currency")]
        public string Currency { get; set; }

        [DataMember(Name = "method")]
        public string Method { get; set; }

        [DataMember(Name = "wallet_name")]
        public string WalletName { get; set; }
    }
}