﻿using System.Runtime.Serialization;

namespace BitfinexClient.Models.Request
{
    [DataContract]
    internal class Request
    {
        [DataMember(Name = "nonce")]
        public string Nonce { get; set; }

        [DataMember(Name = "request")]
        public string Url { get; set; }
    }
}