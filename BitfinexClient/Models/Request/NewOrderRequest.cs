﻿using System.Runtime.Serialization;

namespace BitfinexClient.Models.Request
{
    [DataContract]
    internal class NewOrderRequest : Request
    {
        [DataMember(Name = "symbol")]
        public string Symbol { get; set; }

        [DataMember(Name = "amount")]
        public string Amount { get; set; }

        [DataMember(Name = "price")]
        public string Price { get; set; }

        [DataMember(Name = "exchange")]
        public string Exchange { get; set; }

        [DataMember(Name = "side")]
        public string Side { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        //[DataMember(Name = "is_hidden")]
        public bool IsHidden { get; set; }
    }
}