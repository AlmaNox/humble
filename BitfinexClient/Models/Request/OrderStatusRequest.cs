﻿using System.Runtime.Serialization;

namespace BitfinexClient.Models.Request
{
    [DataContract]
    internal class OrderStatusRequest : Request
    {
        [DataMember(Name = "order_id")]
        public int OrderId { get; set; }
    }
}