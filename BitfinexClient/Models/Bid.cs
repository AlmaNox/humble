﻿using System;
using System.Runtime.Serialization;

namespace BitfinexClient.Models
{
    [DataContract]
    public class Bid
    {
        [DataMember(Name = "price")]
        public String Price { get; set; }

        [DataMember(Name = "amount")]
        public String Amount { get; set; }

        [DataMember(Name = "timestamp")]
        public String Timestamp { get; set; }
    }
}