﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BitfinexClient.Models.Response
{
    [DataContract]
    internal class AllOrdersResponse : ResponseInfo
    {
        public ICollection<OrderResponse> Orders { get; set; }
    }
}