﻿using System.Runtime.Serialization;

namespace BitfinexClient.Models.Response
{
    [DataContract]
    internal class CancelAllOrdersResponse : ResponseInfo
    {
    }
}