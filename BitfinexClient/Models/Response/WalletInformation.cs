﻿using System.Runtime.Serialization;

namespace BitfinexClient.Models.Response
{
    [DataContract]
    internal class WalletInformation
    {
        [DataMember(Name = "currency")]
        public string Currency { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "amount")]
        public string Amount { get; set; }

        [DataMember(Name = "available")]
        public string Available { get; set; }
    }
}