﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BitfinexClient.Models.Response
{
    [DataContract]
    internal class GetBalancesResponse : ResponseInfo
    {
        [DataMember(Name = "collection")]
        public ICollection<WalletInformation> Wallets { get; set; }
    }
}