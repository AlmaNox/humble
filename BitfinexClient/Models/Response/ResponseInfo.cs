﻿using System.Runtime.Serialization;

namespace BitfinexClient.Models.Response
{
    [DataContract]
    internal class ResponseInfo
    {
        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}