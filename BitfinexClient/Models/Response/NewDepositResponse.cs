﻿using System.Runtime.Serialization;

namespace BitfinexClient.Models.Response
{
    [DataContract]
    internal class NewDepositResponse : ResponseInfo
    {
        [DataMember(Name = "result")]
        public string Result { get; set; }

        [DataMember(Name = "method")]
        public string Method { get; set; }

        [DataMember(Name = "currency")]
        public string Currency { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }
    }
}