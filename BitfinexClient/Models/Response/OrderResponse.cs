﻿using System.Runtime.Serialization;

namespace BitfinexClient.Models.Response
{
    [DataContract]
    internal class OrderResponse : ResponseInfo
    {
        [DataMember(Name = "order_id")]
        public int Order { get; set; }

        [DataMember(Name = "symbol")]
        public string Symbol { get; set; }

        [DataMember(Name = "exchange")]
        public string Exchange { get; set; }

        [DataMember(Name = "price")]
        public string Price { get; set; }

        [DataMember(Name = "avg_execution_price")]
        public string AvgExecutionPrice { get; set; }

        [DataMember(Name = "side")]
        public string Side { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "timestamp")]
        public string Timestamp { get; set; }

        [DataMember(Name = "is_live")]
        public string IsLive { get; set; }

        [DataMember(Name = "is_cancelled")]
        public string IsCancelled { get; set; }

        [DataMember(Name = "was_forced")]
        public string WasForced { get; set; }

        [DataMember(Name = "executed_amount")]
        public string ExecutedAmount { get; set; }

        [DataMember(Name = "remaining_amount")]
        public string RemainingAmount { get; set; }

        [DataMember(Name = "original_amount")]
        public string OrginialAmount { get; set; }
    }
}