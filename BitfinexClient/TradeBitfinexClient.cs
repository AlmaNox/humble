﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using ApiConnector;
using ApiConnector.Models;
using AutoMapper;
using BitfinexClient.Models;
using BitfinexClient.Models.Request;
using BitfinexClient.Models.Response;

namespace BitfinexClient
{
    //Say the client wants to make a request to
    //POST https://api.bitfinex.com/v1/order/new
    //With a payload of
    //{
    //"request": "/v1/order/new",
    //"nonce": "1234",
    //"option1": ...
    //}
    //The nonce provided must be strictly increasing.

    //To authenticate a request, use the following:

    //payload = parameters-dictionary -> JSON encode -> base64
    //signature = HMAC-SHA384(payload, api-secret) as hexadecimal
    //send (api-key, payload, signature)
    //These are encoded as HTTP headers named:
    //X-BFX-APIKEY
    //X-BFX-PAYLOAD
    //X-BFX-SIGNATURE
    /// <summary>
    ///     Authentication is done using an API key and a secret. To generate this pair, go to the API page
    /// </summary>
    public class TradeBitfinexClient : TradeClient
    {
        private const string ApiVersion = @"/v1/";
        private const string HttpHeaderApiKey = "X-BFX-APIKEY";
        private const string HttpHeaderPayload = "X-BFX-PAYLOAD";
        private const string HttpHeaderSignature = "X-BFX-SIGNATURE";
        public static string Url = @"https://api.bitfinex.com";

        public TradeBitfinexClient(string apiKey, string secretKey)
        {
            ApiKey = apiKey;
            SecretKey = secretKey;
        }

        #region Private Methods

        /// <summary>
        ///     Przygotowuje informacje do wysłania.
        /// </summary>
        /// <typeparam name="TModel">Typ zwracany przez klasę.</typeparam>
        /// <typeparam name="TRequest">Model danych wysyłanych.</typeparam>
        /// <typeparam name="TResponse">Model danych odbieranych</typeparam>
        /// <param name="url">Polecenie, np "balances" albo "order/new"</param>
        /// <param name="request">Dane, które wysyłamy.</param>
        /// <returns></returns>
        private TModel SendAndBuildRequest<TModel, TRequest, TResponse>(string url, TRequest request)
            where TRequest : Request
        {
            string payload = PreparePayload(url, request);
            string signature = PrepareSignature(payload);
            string responseInPlain = Send(url, payload, signature);
            var response = (TResponse) MySerializer.DeserializeObjectFromJson(typeof (TResponse), responseInPlain);
            return Mapper.Map<TResponse, TModel>(response);
        }

        /// <summary>
        ///     Tworzy payload, który zawiera polecenie w postaci url, noonce czyli licznik, w celu weryfikacji zgłoszenia i
        ///     parametry zapytania.
        /// </summary>
        /// <typeparam name="TOptions">Parametry requesta</typeparam>
        /// <param name="url">Polecenie</param>
        /// <param name="parametrs">Parametry jako model danych wysyłanych.</param>
        /// <returns></returns>
        private static string PreparePayload<TOptions>(string url, TOptions parametrs) where TOptions : Request
        {
            parametrs.Url = ApiVersion + url;
            parametrs.Nonce = Convert.ToString(DateTime.Now.Ticks);

            string payload = MySerializer.SerializeObjectToJson(parametrs);
            return payload;
        }

        /// <summary>
        ///     Hash'uje payload za pomocą secretkey'a w celu weryfikacji.
        /// </summary>
        /// <param name="payload">Payload zapisany za pomocą json'a.</param>
        /// <returns>Zahaszowany payload.</returns>
        private string PrepareSignature(string payload)
        {
            payload = Convert.ToBase64String(Encoding.UTF8.GetBytes(payload));
            var hasher = new HMACSHA384(Encoding.UTF8.GetBytes(SecretKey));
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(payload)))
            {
                byte[] hash = hasher.ComputeHash(ms);
                return BitConverter.ToString(hash).Replace("-", "").ToLower();
            }
        }

        /// <summary>
        ///     Wysyła już przygotowane dane.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="payload">W jsonie.</param>
        /// <param name="signature">Zahaszowana.</param>
        /// <returns></returns>
        private string Send(string url, string payload, string signature)
        {
            var rc = new RestClient(Url + ApiVersion + url, HttpVerb.POST, payload);
            rc.AddHeader(HttpHeaderApiKey, ApiKey);
            rc.AddHeader(HttpHeaderPayload, Convert.ToBase64String(Encoding.UTF8.GetBytes(payload)));
            rc.AddHeader(HttpHeaderSignature, signature);
            return rc.MakeRequest();
        }

        /// <summary>
        /// </summary>
        /// <param name="currency"></param>
        /// <returns></returns>
        private static string ConvertCurrency(Currency currency)
        {
            switch (currency)
            {
                case Currency.BTC:
                    return "BTC";
                case Currency.LTC:
                    return "LTC";
                case Currency.DRK:
                    return "DRK";
            }
            throw new Exception();
        }

        /// <summary>
        /// </summary>
        /// <param name="currency"></param>
        /// <returns></returns>
        private static string ConvertCurrencyToMethod(Currency currency)
        {
            switch (currency)
            {
                case Currency.BTC:
                    return "bitcoin";
                case Currency.LTC:
                    return "litecoin";
                case Currency.DRK:
                    return "darkcoin";
            }
            throw new Exception();
        }

        #endregion

        private string ApiKey { get; set; }
        private string SecretKey { get; set; }

        /// <summary>
        ///     POST /balances
        ///     See your balances.
        ///     Response
        ///     A list of wallet balances:
        ///     type (string): "trading", "deposit" or "exchange".
        ///     currency (string): Currency
        ///     amount (decimal): How much balance of this currency in this wallet
        ///     available (decimal): How much X there is in this wallet that is available to trade.
        /// </summary>
        /// <returns></returns>
        public WalletsStatus GetBalances()
        {
            const string url = @"balances";
            return SendAndBuildRequest<WalletsStatus, BlankRequest, GetBalancesResponse>(url, new BlankRequest());
        }


        /// <summary>
        ///     POST /order/new
        ///     Submit a new order.
        ///     Request
        ///     symbol (string): The name of the symbol (see `/symbols`).
        ///     amount (decimal): Order size: how much to buy or sell.
        ///     price (price): Price to buy or sell at. Must be positive. Use random number for market orders.
        ///     exchange (string): "bitfinex".
        ///     side (string): Either "buy" or "sell".
        ///     type (string): Either "market" / "limit" / "stop" / "trailing-stop" / "fill-or-kill" / "exchange market" /
        ///     "exchange limit" / "exchange stop" / "exchange trailing-stop" / "exchange fill-or-kill".
        ///     (type starting by "exchange " are exchange orders, others are margin trading orders)
        ///     is_hidden (bool) true if the order should be hidden. Default is false.
        ///     Response
        ///     order_id (int): A randomly generated ID for the order.
        ///     and the information given by /order/status
        /// </summary>
        /// <returns></returns>
        public NewOrder MakeNewOrder(Currency firstCurrency, Currency secondCurrency, decimal amount, decimal price,
            bool buy)
        {
            const string url = @"order/new";
            var order = new NewOrderRequest
            {
                Amount = amount.ToString(),
                Exchange = "bitfinex",
                IsHidden = false,
                Price = price.ToString(),
                Side = buy ? Side.buy.ToString() : Side.sell.ToString(),
                Symbol = ConvertCurrency(firstCurrency) + ConvertCurrency(secondCurrency),
                Type = "exchange market" //OrderType.market.ToString(),
            };
            return SendAndBuildRequest<NewOrder, NewOrderRequest, OrderResponse>(url, order);
        }

        /// <summary>
        ///     Cancel order
        ///     POST /order/cancel
        ///     Cancel an order.
        ///     Request
        ///     order_id (int): The order ID given by `/order/new`.
        ///     Response
        ///     Result of /order/status for the cancelled order.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public ActionResult CancelOrder(int orderId)
        {
            const string url = @"order/cancel";
            return SendAndBuildRequest<ActionResult, BlankRequest, CancelOrderResponse>(url, new BlankRequest());
        }

        /// <summary>
        ///     Cancel all active orders
        ///     GET /order/cancel/all
        ///     Cancel all active orders at once.
        ///     Response
        ///     Confirmation of cancellation of all active orders.
        /// </summary>
        /// <returns></returns>
        public ActionResult CancelAllOrders()
        {
            const string url = @"order/cancel/all";
            return SendAndBuildRequest<ActionResult, BlankRequest, CancelAllOrdersResponse>(url, new BlankRequest());
        }

        /// <summary>
        ///     POST /order/status
        ///     Get the status of an order. Is it active? Was it cancelled? To what extent has it been executed? etc.
        ///     Request
        ///     order_id (int): The order ID given by `/order/new`.
        ///     Response
        ///     symbol (string): The symbol name the order belongs to.
        ///     exchange (string): "bitfinex", "bitstamp".
        ///     price (decimal): The price the order was issued at (can be null for market orders).
        ///     avg_execution_price (decimal): The average price at which this order as been executed so far. 0 if the order has
        ///     not been executed at all.
        ///     side (string): Either "buy" or "sell".
        ///     type (string): Either "market" / "limit" / "stop" / "trailing-stop".
        ///     timestamp (time): The timestamp the order was submitted.
        ///     is_live (bool): Could the order still be filled?
        ///     is_cancelled (bool): Has the order been cancelled?
        ///     was_forced (bool): For margin only: true if it was forced by the system.
        ///     executed_amount (decimal): How much of the order has been executed so far in its history?
        ///     remaining_amount (decimal): How much is still remaining to be submitted?
        ///     original_amount (decimal): What was the order originally submitted for?
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public OrderInfo GetOrderStatus(int orderId)
        {
            const string url = @"order/status";
            var order = new OrderStatusRequest();
            return SendAndBuildRequest<OrderInfo, OrderStatusRequest, OrderResponse>(url, order);
        }

        /// <summary>
        ///     POST /orders
        ///     View your active orders.
        ///     Response
        ///     An array of the results of `/order/status` for all your live orders.
        /// </summary>
        /// <returns></returns>
        public OrdersStatus GetAllOrdersStatus()
        {
            const string url = @"orders";
            return SendAndBuildRequest<OrdersStatus, BlankRequest, AllOrdersResponse>(url, new BlankRequest());
        }

        /// <summary>
        ///     New deposit
        ///     POST /deposit/new
        ///     Return your deposit address to make a new deposit
        ///     Request
        ///     currency (string): The currency to deposit in ("BTC", "LTC" or "DRK").
        ///     method (string): Method of deposit (methods accepted: "bitcoin", "litecoin", "darkcoin".
        ///     wallet_name (string): Wallet to deposit in (accepted: "trading", "exchange", "deposit"). Your wallet needs to
        ///     already exist
        ///     Response
        ///     result (string): "success" or "error"
        ///     method (string)
        ///     currency (string)
        ///     address (string): The deposit address (or error message if result = "error")
        /// </summary>
        /// <param name="currency"></param>
        /// <returns></returns>
        public DepositInfo GetNewDepositAddress(Currency currency)
        {
            const string url = @"deposit/new";
            var request = new NewDepositRequest
            {
                Currency = ConvertCurrency(currency),
                Method = ConvertCurrencyToMethod(currency),
                WalletName = Wallet.exchange.ToString(),
            };
            return SendAndBuildRequest<DepositInfo, NewDepositRequest, NewDepositResponse>(url, request);
        }

        private string ConvertWalletToString(Wallet wallet)
        {
            return wallet.ToString();
        }
    }
}